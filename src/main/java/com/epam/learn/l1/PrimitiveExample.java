package com.epam.learn.l1;

public class PrimitiveExample {
    // целые
    // -128 127
    // 8 бит
    byte someByte;
    // -2^15 2^15 - 1
    // 16 бит
    short someShort;
    // -2^31 2^31 - 1
    // 32 бит
    int number = 1;
    // -2^63 2^63 - 1
    // 64 бит
    long bigNumber;

    // дробные
    // https://ru.wikipedia.org/wiki/IEEE_754-2008
    // 32 бита
    float nFloat;
    // 64 бита
    double nDouble;

    // символьный
    // 16 бит
    char someChar;

    // логический
    // https://ru.stackoverflow.com/questions/625400/Размер-типа-boolean
    boolean isTrue;
}
