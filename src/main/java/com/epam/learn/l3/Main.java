package com.epam.learn.l3;

import javax.imageio.IIOException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Main {
    private static boolean isTrue;


    public static void main(String[] args) {
//        String line = null;
//        try {
//            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//            line = reader.readLine();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        int finalResult = Integer.parseInt(Objects.requireNonNull(line));
//
//
//        // если результат отличный от трех - все равно красный
//        if (finalResult == 1) {
//            System.out.println("green");
//        } else if (finalResult == 2) {
//            System.out.println("yellow");
//        } else {
//            System.out.println("red");
//        }
        Colours colours = Colours.RED;
        if (getColourByTime(colours) == Colours.GREEN) {
            System.out.println("GREEN");
        } else if (getColourByTime(colours) == Colours.YELLOW) {
            System.out.println("YELLOW");
        } else if (getColourByTime(colours) == Colours.RED) {
            System.out.println("RED");
        } else {
            throw new RuntimeException();
        }

    }

    private static Colours getColourByTime(Colours colour) {
        return colour;
    }
}
