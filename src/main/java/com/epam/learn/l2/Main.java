package com.epam.learn.l2;

public class Main {
    // +, +=, -, -=, *, *=, %, %=, ++, --, /, /=

    public static void main(String[] args) {
        int result = 6;
        result -= 2;

        System.out.println(result);

        int result2 = 6;
        result2 = result2 - 2;

        // result3 = result3 * 3;
        int result3 = 2;
        result3 *= 3;

        System.out.println(result3);
    }
}
