package com.epam.learn.l2;

public class Main3 {
    public static void main(String[] args) {
        int param1 = 10;
        int param2 = 10;

        // ++ after number - return + 1
        System.out.println(param1++);
        // ++ before number - return + 1
        System.out.println(++param2);

        System.out.println(--param1);
        System.out.println(param1--);
    }

}
