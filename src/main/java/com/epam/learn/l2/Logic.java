package com.epam.learn.l2;

public class Logic {

    //default = false
    private boolean paramA = true;
    private boolean paramB = false;
    private boolean paramC = true;

    public static void main(String[] args) {
        
        Logic logic = new Logic();
        System.out.println(logic.checkResultFirst(logic.paramA, logic.paramB, logic.paramC));

        System.out.println(logic.checkResultSecond(logic.paramA, logic.paramB));
        System.out.println(logic.checkResultThird(logic.paramC, logic.paramB));
        System.out.println(logic.checkResultFourth(logic.paramA, logic.paramB, logic.paramC));
    }

    // &&, ||, >, <, ==, !=, <=, >=

    // A && B || C

    private boolean checkResultFirst(boolean a, boolean b, boolean c) {
        return a && b || c;
    }

    // A && B
    // C || B
    // A || B && C

    private boolean checkResultSecond(boolean a, boolean b) {
        return a && b;
    }

    private boolean checkResultThird(boolean a, boolean b) {
        return a || b;
    }

    private boolean checkResultFourth(boolean a, boolean b, boolean c) {
        return a || b && c;
    }

}
